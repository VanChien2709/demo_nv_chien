package Chien.demo;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManagerService {
	 @Autowired
	    private ManagerRepository repo;
	     
	    public List<manager> listAll(){
	    	return repo.findAll();
	    }
	    public void save(manager manager) {
	        repo.save(manager);
	    }
	    public manager get(long id) {
	        return repo.findById(id).get();
	    }
	    public void delete(long id) {
	        repo.deleteById(id);
	    }
}
