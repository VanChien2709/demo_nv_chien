package Chien.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VanchienApplication {

	public static void main(String[] args) {
		SpringApplication.run(VanchienApplication.class, args);
	}
}
