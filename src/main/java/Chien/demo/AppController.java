package Chien.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AppController {
	@Autowired
    private ManagerService service;
    @GetMapping(value="/final")
    public String viewHomePage(Model model) {
    	List<manager> listManager = service.listAll();
    	model.addAttribute("listmanager",listManager);
    	System.out.println(service.listAll().get(0).getName()+"===============");
    	return "index";
    }
    @GetMapping(value="/abc")
    public String showNewProductPage(Model model) {
    	manager manager = new manager();
        model.addAttribute("manager",manager);
        return "new_manager";
    }
//    @RequestMapping(value = "/save", method = RequestMethod.POST)
//    public String saveProduct(@ModelAttribute("manager") manager manager) {
//    	
//        service.save(manager);
//        return "redirect:/";
//    }
}
